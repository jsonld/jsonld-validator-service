# jsonld-validator-service

This is a Spring-Boot based project that exposes the [JSON-LD Validator library](https://gitlab.com/jsonld/jsonld-validator)
as a microservice.

To run this project, type:

```
$ mvn spring-boot:run
```

To dockerise this project, type:

```
docker build -t jsonld/jsonld-validator-service:latest .
```