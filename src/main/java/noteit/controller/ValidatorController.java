package noteit.controller;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import jsonld.JsonLdValidator;

@RestController
public class ValidatorController {
    private JsonLdValidator validator = new JsonLdValidator();

    @PostMapping("/validate")
    @ResponseStatus(HttpStatus.OK)
    public void validate(@RequestBody String jsonString) throws JsonParseException, IOException {
        validator.validate(jsonString);
    }
}